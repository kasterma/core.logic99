(ns core.logic99.testbintrees
  (:require [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use core.logic99.bintrees
        midje.sweet))

(facts "leaf"
       (logic/run 1 [q] (leaf? [:a nil nil])) => '(_0)
       (logic/run 1 [q] (leaf? [:a nil])) => '()
       (logic/run 1 [q] (leaf? [:a nil []])) => '()
       (logic/run 1 [q] (leaf? (leaf :hi))) => '(_0))

(facts "bintree"
       (logic/run 1 [q] (bintree? nil)) => '(_0)
       (logic/run 1 [q] (bintree? [nil nil nil])) => '(_0)
       (logic/run 1 [q] (bintree? [:a [:b nil nil] [:c nil nil]])) => '(_0)
       (logic/run 1 [q] (bintree? [:a (leaf :b) (leaf :c)])) => '(_0)
       (logic/run 1 [q] (bintree? [:a (leaf :b) (leaf :c) (leaf :d)])) => '()
       (logic/run 1 [q] (bintree? [:a :b])) => '())

(facts "generate balanced bin trees"
  (logic/run* [q] (balbintree q 1)) => '([:x nil nil])
  (logic/run* [q] (balbintree q 2)) => '([:x nil [:x nil nil]] [:x [:x nil nil] nil])
  (logic/run* [q] (balbintree q 3)) => '([:x [:x nil nil] [:x nil nil]])
  (logic/run* [q] (balbintree q 5)) =>
  '([:x [:x nil [:x nil nil]]
        [:x nil [:x nil nil]]]
    [:x [:x nil [:x nil nil]]
        [:x [:x nil nil] nil]]
    [:x [:x [:x nil nil] nil]
        [:x nil [:x nil nil]]]
    [:x [:x [:x nil nil] nil]
        [:x [:x nil nil] nil]]))

(facts "symmetric binary trees"
  (logic/run* [q] (symmbintree? [:x [:x nil nil] [:x nil [:x nil nil]]])) => '()
  (logic/run* [q] (symmbintree? [:x [:x nil [:x nil nil]] [:x [:x nil nil] nil]])) => '(_0))

(facts
  (logic/run 1 [q] (insertnum 1 (leaf 2) q)) => '([2 [1 nil nil] nil])
  (logic/run 1 [q] (insertnum 1 (leaf 0) q)) => '([0 nil [1 nil nil]])
  (logic/run 1 [q] (insertnum 2 [0 nil [1 nil nil]] q)) => '([0 nil [1 nil [2 nil nil]]])
  (logic/run 1 [q] (insertnum 0.5 [0 nil [1 nil nil]] q)) => '([0 nil [1 [0.5 nil nil] nil]]))

(facts
  (logic/run 1 [q] (searchtree [1 2 3] q)) => '([1 nil [2 nil [3 nil nil]]])
  (logic/run 1 [q] (searchtree [1 3 2] q)) => '([1 nil [3 [2 nil nil] nil]]))

(facts
  (logic/run* [q] (leafcounto [4 (leaf 3) nil] q)) => '(3)
  (logic/run* [q] (leafcounto (leaf 3) q)) => '(2)
)
