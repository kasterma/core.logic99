(ns core.logic99.arithtest
  (:require [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use core.logic99.arith
        midje.sweet))

(facts "prime?"
  (prime? 7) => true
  (prime? 6) => false
  (prime? 42) => false
  (prime? 43) => true
  (prime? 36) => false
  (prime? 37) => true)

(comment
  (facts "list-factors"
    (logic/run 1 [q] (list-factors 4 [2 2])) => '(_0)
    (logic/run 1 [q] (list-factors 10 [2 5])) => '(_0)
    (logic/run 1 [q] (list-factors 24 [2 2 2 3])) => '(_0)))
