(ns core.logic99.testlog
  (:require [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use core.logic99.log
        midje.sweet))

(facts "table"
  (table x y (OR x y)) => '([true true true] [true false true] [false true true])
  (table x y (AND (OR x y) y)) => '([true true true] [false true true]))

(facts "graycodeo"
  (logic/run 1 [q] (graycodeo 1 q)) => '([[0] [1]])
  (logic/run 1 [q] (graycodeo 2 q)) => '(((0 0) (0 1) (1 1) (1 0)))
  (logic/run 1 [q] (graycodeo 3 q)) => '(((0 0 0) (0 0 1) (0 1 1) (0 1 0) (1 1 0) (1 1 1) (1 0 1) (1 0 0))))

(facts
  (logic/run 1 [q] (prependo [[]] q 1)) => '([[1]]))

(facts "insertfreqsorto"
  (logic/run 1 [q] (insertfreqsorto [:a 1] [] q)) => '([[:a 1]])
  (logic/run 1 [q] (insertfreqsorto [:a 0] [] q)) => '([[:a 0]])
  (logic/run 1 [q] (insertfreqsorto [:a 1] [[:b 0]] q)) => '([[:b 0] [:a 1]])
  (logic/run 1 [q] (insertfreqsorto [:a 1] [[:b 2]] q)) => '([[:a 1] [:b 2]])
  (logic/run 1 [q] (insertfreqsorto [:a 1] [[:c 0] [:b 2]] q)) => '([[:c 0] [:a 1] [:b 2]])
  (logic/run 1 [q] (insertfreqsorto [:a 1] [[:b 2] [:c 3]] q)) => '([[:a 1] [:b 2] [:c 3]])
  (logic/run 1 [q] (insertfreqsorto [:a 2] [[:b 1] [:c 3]] q)) => '(([:b 1] [:a 2] [:c 3]))
  (logic/run 1 [q] (insertfreqsorto [:a 0] [[:b 1] [:c 9]] q)) => '(([:a 0] [:b 1] [:c 9]))
  (logic/run 1 [q] (insertfreqsorto [:a 10] [[:b 1] [:c 9]] q)) => '(([:b 1] [:c 9] [:a 10]))
  (logic/run 1 [q] (insertfreqsorto [:e 9] [[:f 5]] q)) => '(([:f 5] [:e 9]))
  (logic/run 1 [q] (insertfreqsorto [:d 16] [[:f 5] [:e 9]] q)) => '(([:f 5] [:e 9] [:d 16])))

(facts "sortfreqs"
  (logic/run 1 [q] (sortfreqs [[:a 45] [:b 13] [:c 12] [:d 16] [:e 9] [:f 5]] q))
  => '(([:f 5] [:e 9] [:c 12] [:b 13] [:d 16] [:a 45])))

(facts "make-tree"
  (logic/run 1 [q] (make-tree [[:treeeee 1]] q)) => '(:treeeee)
  (logic/run 1 [q] (make-tree [[:a 1] [:b 2] [:c 3]] q)) => '([:c [:a :b]])
  (logic/run 1 [q] (make-tree [[:a 2] [:b 2] [:c 3] [:d 3]] q)) => '([[:a :b] [:d :c]])
  (logic/run 1 [q] (make-tree [[:f 5] [:e 9] [:c 12] [:b 13] [:d 16] [:a 45]] q)) => '([:a [[:c :b] [[:f :e] :d]]]))

(facts "combinesmallest"
  (logic/run 1 [q] (combinesmallest [[:a 2] [:b 3]] q)) => '(([[:a :b] 5]))
  (logic/run 1 [q] (combinesmallest [[:a 2] [:b 3] [:c 4]] q)) => '(([:c 4] [[:a :b] 5])))

(facts "add-label"
  (logic/run 1 [q] (add-label [[:a [1]] [:b [1 2 3]]] q :HIHO)) => '(([:a (:HIHO 1)] [:b (:HIHO 1 2 3)])))

(facts "read-tree"
  (logic/run 1 [q] (read-tree [:a] q)) => '([[:a [0]]])
  (logic/run 1 [q] (read-tree [:a :b] q)) => '([[:a [0]] [:b [1]]])
  (logic/run 1 [q] (read-tree [:c [:a :b]] q)) => '([[:c [0]] [:a [1 0]] [:b [1 1]]] )
  (logic/run 1 [q] (read-tree [[:a :b] [:d :c]] q)) => '([[:a [0 0]] [:b [0 1]] [:d [1 0]] [:c [1 1]]]))

(facts "huffman"
  (logic/run 1 [q] (huffmancodeo [[:a 45] [:b 13] [:c 12] [:d 16] [:e 9] [:f 5]] q))
  => '(([:a [0]] [:c (1 0 0)] [:b (1 0 1)] [:f (1 1 0 0)] [:e (1 1 0 1)] [:d (1 1 1)]))
  )
