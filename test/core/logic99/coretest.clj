(ns core.logic99.coretest
  (:require [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use core.logic99.core
        midje.sweet))

(facts
  (logic/run* [q] (initsegmento [1 2] [1 2 3])) => '(_0)
  (logic/run* [q] (initsegmento [1 2] [2 3 4])) => '()
;; is correct but doesn't match
;;  (logic/run 2 [q] (initsegmento [1 2] q)) => '((1 2 . _0))
  )

(facts
  (logic/run* [q] (overlapo [1 2 3] [2 3 4])) => '(_0)
  (logic/run* [q] (overlapo [1 2 3] [5 2 3 4])) => '()
  (logic/run* [q] (overlapo [1 1 1] [1 1 3])) => '(_0 _0)
  (logic/run 1 [q] (overlapo [1 1 1] [1 1 3])) => '(_0)
  (logic/run 6 [q] (overlapo q [1 1 3])) => '((1) (1 1) (_0 1) (1 1 3) (_0 1 1) (_0 _1 1))
;  (logic/run* [q] (overlapo [1 1 1] q)) => '((1 1 1 . _0) (1 1 . _0) (1 . _0))
  )

(facts "lasto"
  (logic/run 3 [q] (lasto q 3)) => '((3) (_0 3) (_0 _1 3))
  (logic/run* [q] (lasto [1 2 3 4 5] q)) => '(5)
  (logic/run* [q] (lasto [1 2 3] 3)) => '(_0)
  (logic/run* [q] (lasto [1 2 3] 2)) => '())

(facts "lasto2"
  (logic/run 3 [q] (lasto2 q 3)) => '((3) (_0 3) (_0 _1 3))
  (logic/run* [q] (lasto2 [1 2 3 4 5] q)) => '(5)
  (logic/run* [q] (lasto2 [1 2 3] 3)) => '(_0)
  (logic/run* [q] (lasto2 [1 2 3] 2)) => '())

(facts "lasto3"
  (logic/run 3 [q] (lasto3 q 3)) => '((3) (_0 3) (_0 _1 3))
  (logic/run* [q] (lasto3 [1 2 3 4 5] q)) => '(5)
  (logic/run* [q] (lasto3 [1 2 3] 3)) => '(_0)
  (logic/run* [q] (lasto3 [1 2 3] 2)) => '())

(facts "singeltono"
  (logic/run* [q] (singletono [1 2 3])) => '()
  (logic/run* [q] (singletono [1 2])) => '()
  (logic/run* [q] (singletono [3])) => '(_0)
  (logic/run* [q] (singletono [])) => '()
  (logic/run* [q] (singletono '(3))) => '(_0)
  (logic/run* [q] (singletono '(2 3))) => '()
  (logic/run* [q] (singletono q)) => '((_0)))

(facts "next2lasto"
  (logic/run* [q] (next2lasto [1 2 3] 2)) => '(_0)
  (logic/run 3 [q] (next2lasto q 2))
  => '((2 _0) (_0 2 _1) (_0 _1 2 _2))
  (logic/run* [q] (next2lasto [1 2 3 4 5] q)) => '(4)
  (logic/run* [q] (next2lasto2 [1 2 3] 2)) => '(_0)
  (logic/run* [q] (next2lasto2 [1 2 3] 1)) => '()
  (logic/run* [q] (next2lasto2 [1 2 3] q)) => '(2))

(facts "next2lasto"
  (logic/run* [q] (next2lasto3 [1 2 3] 2)) => '(_0)
  (logic/run 3 [q] (next2lasto3 q 2))
  => '((2 _0) (_0 2 _1) (_0 _1 2 _2))
  (logic/run* [q] (next2lasto3 [1 2 3 4 5] q)) => '(4)
  (logic/run* [q] (next2lasto3 [1 2 3] 2)) => '(_0)
  (logic/run* [q] (next2lasto3 [1 2 3] 1)) => '()
  (logic/run* [q] (next2lasto3 [1 2 3] q)) => '(2))

(facts "next2lasto"
  (logic/run* [q] (next2lasto4 [1 2 3] 2)) => '(_0)
  (logic/run 3 [q] (next2lasto4 q 2))
  => '((2 _0) (_0 2 _1) (_0 _1 2 _2))
  (logic/run* [q] (next2lasto4 [1 2 3 4 5] q)) => '(4)
  (logic/run* [q] (next2lasto4 [1 2 3] 2)) => '(_0)
  (logic/run* [q] (next2lasto4 [1 2 3] 1)) => '()
  (logic/run* [q] (next2lasto4 [1 2 3] q)) => '(2))

(facts "lengtho"
  (logic/run* [q] (lengtho [1 2 3] q)) => '(3)
  (logic/run 1 [q] (lengtho q 2)) => '((_0 _1))
  (logic/run 2 [q] (logic/fresh [l lh] (lengtho l lh) (logic/== [l lh] q))) => '([() 0] [(_0) 1]))

(facts "lengtho"
  (logic/run* [q] (lengtho2 [1 2 3] q)) => '(3)
  (logic/run 1 [q] (lengtho2 q 2)) => '((_0 _1))
  (logic/run 2 [q] (logic/fresh [l lh] (lengtho2 l lh) (logic/== [l lh] q))) => '([() 0] [(_0) 1]))


(facts "ktho"
  (logic/run* [q] (ktho [1 2 3] 2 q)) => '(1)
;; the next statement runs correct, but doesn't test well
;;  (logic/run 2 [q] (ktho q 2 1)) => '((_0 2) (_0 2 _0))
  (logic/run 2 [q] (ktho [1 2 1 2] 2 q)) => '(1 3)
  (logic/run* [q] (ktho [3 2 1] q 1)) => '(2))

(facts "concato"
  (logic/run* [q] (concato [] [3 4] q)) => '((3 4))
  (logic/run* [q] (concato [1] [3 4] q)) => '((1 3 4))
  (logic/run* [q] (concato [1 2] [3 4] q)) => '((1 2 3 4))
  (logic/run* [q] (concato [1 2] q [1 2 3 4])) => '((3 4))
  (logic/run* [q] (concato q [3 4] [1 2 3 4])) => '((1 2))
  (logic/run 1 [q] (concato q q q)) => '(())
  ;;  (logic/run 2 [q] (concato q q q))    ;; <= diverges
  ;; should be avoidable, concatenating sums lengths, easy to see only empty can satisfy thi
  )

(facts "concato"
  (logic/run* [q] (concato2 [] [3 4] q)) => '((3 4))
  (logic/run* [q] (concato2 [1] [3 4] q)) => '((1 3 4))
  (logic/run* [q] (concato2 [1 2] [3 4] q)) => '((1 2 3 4))
  (logic/run* [q] (concato2 [1 2] q [1 2 3 4])) => '((3 4))
  (logic/run* [q] (concato2 q [3 4] [1 2 3 4])) => '((1 2))
  (logic/run 1 [q] (concato2 q q q)) => '(())
  ;;  (logic/run 2 [q] (concato2 q q q))    ;; <= diverges
  ;; should be avoidable, concatenating sums lengths, easy to see only empty can satisfy thi
  )

(facts "reverso"
  (logic/run* [q] (reverso [3 2 1] [1 2 3])) => '(_0)
  (logic/run* [q] (reverso q [1 2 3])) => '((3 2 1))
  (logic/run 1 [q] (reverso [1 2 3] q)) => '((3 2 1))   ;; <= diverges with a no bigger than 1
  (logic/run 1 [q] (reverso q q)) => '((_0))
  (logic/run 2 [q] (reverso q q)) => '((_0) (_0 _0))
  (logic/run 3 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0))
  (logic/run 4 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0) (_0 _1 _1 _0))
  (logic/run 5 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0) (_0 _1 _1 _0) (_0 _1 _2 _1 _0)))

(facts "reverso"
  (logic/run* [q] (reverso [3 2 1] [1 2 3])) => '(_0)
  (logic/run* [q] (reverso q [1 2 3])) => '((3 2 1))
  (logic/run 1 [q] (reverso [1 2 3] q)) => '((3 2 1))   ;; <= diverges with a no bigger than 1
  (logic/run 1 [q] (reverso q q)) => '((_0))
  (logic/run 2 [q] (reverso q q)) => '((_0) (_0 _0))
  (logic/run 3 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0))
  (logic/run 4 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0) (_0 _1 _1 _0))
  (logic/run 5 [q] (reverso q q)) => '((_0) (_0 _0) (_0 _1 _0) (_0 _1 _1 _0) (_0 _1 _2 _1 _0)))


(facts "palindromo"
  (logic/run* [q] (palindromo [1 2 1])) => '(_0)
  (logic/run* [q] (palindromo [1 2 2])) => '()
  (logic/run 5 [q] (palindromo q)) => '((_0) (_0 _0) (_0 _1 _0) (_0 _1 _1 _0) (_0 _1 _2 _1 _0))
  (logic/run 1 [q] (palindromo q) (lengtho q 3) (logic/firsto q 1)) => '((1 _0 1)))  ;; <= diverges with a no bigger than 1

(facts "notlisto"
  (logic/run* [q] (notlisto 1)) => '(_0)
  (logic/run* [q] (notlisto [1])) => '()
  (logic/run* [q] (notlisto nil)) => '(_0))

(facts "flato"
  (logic/run* [q] (flato [1 2 3 4])) => '(_0)
  (logic/run* [q] (flato [[1] 2 3 4])) => '()
  (logic/run* [q] (flato [1 2 [3 4]])) => '()
  (logic/run* [q] (flato [[1] 2 [3 4]])) => '()
  (logic/run* [q] (flato 1)) => '(_0))

(facts "flatteno"
  ;; these tests find the right answer, but many times.

  ;; (logic/run* [q] (flatteno [1 2 3 4] [1 2 3 4])) => '(_0)
  ;; (logic/run* [q] (flatteno [[1 2] 3 4 5 6] q)) => '((1 2 3 4 5 6))
  ;; (logic/run* [q] (flatteno [[1 2] [3] 4 [5] 6] q)) => '((1 2 3 4 5 6))
  ;; (logic/run* [q] (flatteno [[1 2] [3 [4 5]] [ 6]] q)) => '((1 2 3 4 5 6))


  ;; (logic/run 1 [q] (flatteno q [1 2 3 4])) => '((1 2 3 4))   ;; <= diverges
)

(facts "dedupo"
  (logic/run* [q] (dedupo [1 2] q)) => '((1 2))
  (logic/run* [q] (dedupo [1] q)) => '([1])
  (logic/run* [q] (dedupo [1 1 2] q)) => '((1 2))
  (logic/run* [q] (dedupo [1 1 2 1] q)) => '((1 2 1))
  (logic/run* [q] (dedupo [1 1 2 1 3 3] q)) => '((1 2 1 3))
  (logic/run 1 [q] (dedupo q [1 2 3])) => '((1 2 3))
  (logic/run 2 [q] (dedupo q [1 2 3])) => '((1 2 3) (1 1 2 3))
  (logic/run 3 [q] (dedupo q [1 2 3])) => '((1 2 3) (1 1 2 3) (1 2 2 3))
  (logic/run 5 [q] (dedupo q [1 2 3])) => '((1 2 3) (1 1 2 3) (1 2 2 3) (1 2 3 3) (1 1 1 2 3)))

(facts "packdupo"
  (logic/run 1 [q] (packdupo [1 1 2 1 2 2] q)) => '(((1 1) (2) (1) (2 2)))
  (logic/run 1 [q] (packdupo [1 1 2] q)) => '(((1 1) (2)))
  (logic/run 1 [q] (packdupo [1] q)) => '(((1)))
  (logic/run 1 [q] (packdupo [] q)) => '(())
  (logic/run 1 [q] (packdupo q [[1]])) => '((1))
;;  (logic/run 2 [q] (packdupo q [[1]]))  ;; <= diverges
  (logic/run 1 [q] (packdupo q [[1 1] [2 2]])) => '((1 1 2 2))
  (logic/run 1 [q] (packdupo q [[1 1] [2 2] [1]])) => '((1 1 2 2 1)))

(facts "runleno"
   (logic/run* [q] (runleno [1 1 2 2 2 1 1] q)) => '(((2 1) (3 2) (2 1)))
   (logic/run 1 [q] (runleno q '((2 1) (3 2) (2 1)))) => '((1 1 2 2 2 1 1)))

(facts "runleno modified"
  (logic/run 1 [q] (mrunleno q '((2 :a) :b (2 :c) :a))) => '((:a :a :b :c :c :a))
  (logic/run 1 [q] (mrunleno [:a :a :b :c :c :a] q)) => '(((2 :a) :b (2 :c) :a)))

(facts "repo"
   (logic/run 1 [q] (repo :a 3 q)) => '((:a :a :a))
   (logic/run 1 [q] (repo :a q [:a :a])) => '(2)
   (logic/run 1 [q] (repo q 2 [:a :a])) => '(:a)
   (logic/run 1 [q] (logic/fresh [el len] (repo el len q))) => '(())
   (logic/run 2 [q] (logic/fresh [el len] (repo el len q))) => '(() (_0))
   (logic/run 3 [q] (logic/fresh [el len] (repo el len q))) => '(() (_0) (_0 _0))
   (logic/run 5 [q] (logic/fresh [el len] (repo el len q))) => '(() (_0) (_0 _0) (_0 _0 _0) (_0 _0 _0 _0))
   (logic/run 5 [q] (logic/fresh [len] (repo :a len q))) => '(() (:a) (:a :a) (:a :a :a) (:a :a :a :a)))

(facts "dupelto"
  (logic/run 1 [q] (dupelto [1 1] q)) => '((1 1 1 1))
  (logic/run 1 [q] (dupelto [1 2 3] q)) => '((1 1 2 2 3 3))
  (logic/run 1 [q] (dupelto q [1 1 :a :a])) => '((1 :a)))


(facts "multelto"
  (logic/run 1 [q] (multelto [1 1] q 3)) => '((1 1 1 1 1 1))
  (logic/run 1 [q] (multelto [1 2 3] q 4)) => '((1 1 1 1 2 2 2 2 3 3 3 3))
  (logic/run 1 [q] (multelto q [1 1 1 :a :a :a] 3)) => '((1 :a)))

(facts "dropntho"
  (logic/run 1 [q] (dropntho [1 2 3 4 5 6 7] q 2)) => '((1 2 4 5 7))
  (logic/run 1 [q] (dropntho [1 2 3 4 5 6 7] q 3)) => '((1 2 3 5 6 7))
  (logic/run 1 [q] (dropntho [1 2 3 4 5 6 7] q 1)) => '((1 3 5 7))
  (logic/run 1 [q] (dropntho [1 2 3 4 5 6 7] q 0)) => '(())
  (logic/run 1 [q] (dropntho [1 2 3 4 5 6 7] q 1)) => '((1 3 5 7))
  (logic/run 1 [q] (dropntho q [1 2 3 4 5 6 7] 3)) => '((1 2 3 _0 4 5 6 _1 7)))

(facts "sliceo"
  (logic/run 1 [q] (sliceo [1 2 3 4 5] q 2 2)) => '((3))
  (logic/run 1 [q] (sliceo [1 2 3 4 5] q 3 4)) => '((4 5))
  (logic/run 1 [q] (sliceo [1 2 3 4 5] q 1 7)) => '()
  (logic/run 1 [q] (sliceo [1 2 3 4 5] q 3 4)) => '((4 5))
  (logic/run 1 [q] (sliceo [1 2 3 4 5] q 1 4)) => '((2 3 4 5))
;; next two are computed correctly, but in testing are somehow not considered equal
;;  (logic/run 1 [q] (sliceo q [1 2 3 4] 1 4)) => '((_0 1 2 3 4 . _1))
;;  (logic/run 2 [q] (sliceo q [1 2 3 4] 1 4)) => '((_0 1 2 3 4 . _1))
  )

(facts "splito"
     (logic/run 1 [q] (splito [1 2 3 4 5 6] 2 q)) => '(((1 2 3) (4 5 6)))
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 1 q)) => '(((1 2) (3 4 5 6)))
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 0 q)) => '(((1) (2 3 4 5 6)))
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 9 q)) => '()
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 7 q)) => '()
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 6 q)) => '()
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 5 q)) => '(((1 2 3 4 5 6) ()))
   (logic/run 1 [q] (splito [1 2 3 4 5 6] -1 q)) => '()
   (logic/run 1 [q] (splito [1 2 3 4 5 6] 0 q)) => '(((1) (2 3 4 5 6)))
   (logic/run 1 [q] (splito [1 2 3 4 5 6] q [[1 2] [3 4 5 6]])) => '(1)
   (logic/run 1 [q] (splito [1 2 3 4 5 6] q [[1 2 3] [4 5 6]])) => '(2)
   (logic/run 1 [q] (splito [1 2 3 4 5 6] q [[1 2 3] [4 5 6 7]])) => '())

(facts "rotato"
  (logic/run 1 [q] (rotato [1 2 3 4 5 6] q 2)) => '((4 5 6 1 2 3))
  (logic/run 1 [q] (rotato [1 2 3 4 5 6] q 1)) => '((3 4 5 6 1 2))
  (logic/run 1 [q] (rotato [1 2 3 4 5 6] q 0)) => '((2 3 4 5 6 1))
  (logic/run 1 [q] (rotato [1 2 3 4 5 6] q 4)) => '((6 1 2 3 4 5))
  (logic/run 1 [q] (rotato [1 2 3 4 5 6] q 5)) => '((1 2 3 4 5 6))
  (logic/run 1 [q] (rotato q [1 2 3 4 5 6] 5)) => '((1 2 3 4 5 6))
  (logic/run 1 [q] (rotato q [1 2 3 4 5 6] 3)) => '((3 4 5 6 1 2)))

(facts "removektho"
  (logic/run* [q] (removektho [1 2 3] q 0)) => '((2 3))
  (logic/run* [q] (removektho [1 2 3] q 1)) => '((1 3))
  (logic/run* [q] (removektho [1 2 3] q 2)) => '((1 2))
  (logic/run* [q] (removektho q [1 2 3] 2)) => '((1 2 _0 3)))

(facts "insertktho"
  (logic/run* [q] (insertktho [1 2 3] q :a 5)) => '()
  (logic/run* [q] (insertktho [1 2 3] q :a 3)) => '((1 2 3 :a))
  (logic/run* [q] (insertktho [1 2 3] q :a 2)) => '((1 2 :a 3))
  (logic/run* [q] (insertktho q[1 2 3] :a 2)) => '()
  (logic/run* [q] (insertktho q [1 :a 2 3] :a 2)) => '()
  (logic/run* [q] (insertktho q [1 2 :a 3] :a 2)) => '((1 2 3)))

(facts "rangeo"
  (logic/run* [q] (rangeo q 2 8)) => '((2 3 4 5 6 7 8))
  (logic/run* [q] (rangeo q 4 8)) => '((4 5 6 7 8))
  (logic/run* [q] (rangeo [4 5 6 7] 4 q)) => '(7)
  ;; (logic/run* [q] (rangeo [4 5 6 7] q 7))   ;; <= diverges
  (logic/run 1 [q] (rangeo [4 5 6 7] q 7)) => '(4))

(facts "combo"
  (logic/run* [q] (combo [1 2 3] q 2)) => '((1 2) (1 3) (2 3))
  (logic/run* [q] (combo [1 2 3] q 1)) => '((1) (2) (3)))

(facts "combo2"
  (logic/run 20 [q] (combo2 [1 2 3] q 2)) => '((2 1) (3 1) (3 2))
  (logic/run 20 [q] (combo2 [1 2 3] q 1)) => '((1) (2) (3)))

(facts "addelemo"
  (logic/run 4 [q] (addelemo :elem [[1] [2 3] [3]] q [2 2 2])) => '(((:elem 1) [2 3] [3]) ([1] [2 3] (:elem 3)))
  (logic/run 3 [q] (addelemo :elem [[1] [2] [3]] q [2 2 2])) => '(((:elem 1) [2] [3]) ([1] (:elem 2) [3]) ([1] [2] (:elem 3)))
  (logic/run 4 [q] (addelemo :elem [[1] [2] [3]] q [2 2 2])) => '(((:elem 1) [2] [3]) ([1] (:elem 2) [3]) ([1] [2] (:elem 3))))

(facts "groupo"
  (logic/run 2 [q] (groupo [1 2 3 4 5] q [2 3])) => '(((2 1) (5 4 3)) ((3 1) (5 4 2)))
  (logic/run* [q] (groupo [1 2 3 4 5] q [2 3])) => '(((2 1) (5 4 3)) ((3 1) (5 4 2)) ((4 1) (5 3 2)) ((3 2) (5 4 1)) ((5 1) (4 3 2)) ((5 4) (3 2 1)) ((4 2) (5 3 1)) ((4 3) (5 2 1)) ((5 2) (4 3 1)) ((5 3) (4 2 1))))

(def ll1 [[2 3] [1]])
(def ll2 [[2 3] [4 5 6] [9]])

(facts "shortero"
  (logic/run* [q] (shortero [1 2] [1 2 3])) => '(_0)
  (logic/run* [q] (shortero [1 2] [1 2])) => '(_0)
  (logic/run* [q] (shortero [1 2] [1])) => '()
  (logic/run* [q] (shortero [] [1])) => '(_0))

(facts "insertsorto"
  (logic/run* [q] (insertsorto [1 2] [[1] [2 3] [1 2 3]] q)) => '(([1] [1 2] [2 3] [1 2 3]))
  (logic/run* [q] (insertsorto [1 2] [[1] [3] [1 2 3]] q)) => '(([1] [3] [1 2] [1 2 3])))

(facts "sortsizeo"
  (logic/run 1 [q] (sortsizeo ll1 q)) => '(([1] [2 3]))
  (logic/run 1 [q] (sortsizeo ll2 q)) => '(([9] [2 3] [4 5 6])))
