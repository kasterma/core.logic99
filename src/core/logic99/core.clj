(ns core.logic99.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use midje.sweet)
  (:gen-class))

;; some examples of inequality constraints

(logic/run* [q] (logic/!= q 3)) ; ((_0 :- (!= (_0 3))))
(logic/run* [q] (logic/fresh [a b]
              (logic/== [a b] q)
              (logic/!= 3 a)
              (logic/== 3 b))) ; (([_0 3] :- (!= (_0 3))))

;; in the following two notice the difference between simulatneous
;; ineqaulity constraints, and separate inequality contraints.
(logic/run* [q] (logic/fresh [a b]
              (logic/== [a b] q)
              (logic/!= [5 6] q))) ;(([_0 _1] :- (!= (_1 6) (_0 5))))

(logic/run* [q] (logic/fresh [a b]
              (logic/== [a b] q)
              (logic/!= a 5)
              (logic/!= b 6))) ;(([_0 _1] :- (!= (_1 6)) (!= (_0 5))))

;; contraints simplify where possible
(logic/run* [q] (logic/fresh [a b]
              (logic/log a)
              (logic/trace-s)
              (logic/== [a b] q)
              (logic/!= [5 6] q)
              (logic/== a 5)))  ;(([5 _0] :- (!= (_0 6))))

;; debugging tools
;;                (logic/trace-s)
;;               (logic/trace-lvars "herere" r h)

(logic/defne initsegmento [is l]
  ([[his . tis] [his . tl]]
     (logic/conde
      ((logic/emptyo tis))
      ((initsegmento tis tl)))))

(logic/defne overlapo
  "some final segment of l1 is an initial segment of l2"
  [l1 l2]
  ([_ _] (initsegmento l1 l2))
  ([[h1 . t1] _] (overlapo t1 l2)))

;; # Solving some of the problems from Prolog 99 problems in core.logic
;;
;; dnolen: some of the later problems might not be possible yet.

;; 1.01
(defn lasto
  "succeeds if e is the last element of the list l"
  [l e]
  (logic/fresh [h r]
               (logic/conso h r l)
               (logic/conde
                [(logic/emptyo r)
                 (logic/== h e)]
                [(lasto r e)])))

(logic/defne lasto2
  [l e]
  ([[e . '()] _])
  ([[ht . tl] _] (lasto2 tl e)))

(logic/defne lasto3
  [l e]
  ([[e . tl] _] (logic/emptyo tl))
  ([[ht . tl] _] (lasto3 tl e)))

;; conso introduces an LCons if needed that directs the unification

;; 1.02
(defn singletono
  [l]
  (logic/fresh [h r]
               (logic/conso h r l)
               (logic/emptyo r)))

(logic/defne singletono2 [l] ([[hd . '()]]))


(defn next2lasto
  "succeeds if e is the next to last element of the list l"
  [l e]
  (logic/fresh [h r]
               (logic/conso h r l)
               (logic/conde
                [(singletono r)
                 (logic/== h e)]
                [(next2lasto r e)])))

(defn next2lasto2
  [l e]
  (logic/fresh [h r]
               (logic/conso h r l)
               (logic/conde
                [(logic/fresh [hh rr] (logic/conso hh rr r)
                              (logic/emptyo rr)
                              (logic/== h e))]
                [(next2lasto2 r e)])))

(logic/defne next2lasto3 [l e]
  ([[e . r] _]
     (singletono2 r))
  ([[_ . r] _]
     (next2lasto3 r e)))

(logic/defne next2lasto4 [l e]
  ([[e . r . '()] _])
  ([[_ . r] _]
     (next2lasto4 r e)))

;; 1.04
(defn lengtho
  ([l k]
     (lengtho l k 0))
  ([l k i]
     (logic/conde [(logic/== k i) (logic/emptyo l)]
              [(logic/fresh [h r]
                 (logic/conso h r l)
                 (lengtho r k (inc i)))])))

(defn lengtho2
  ([l k]
     (lengtho l k 0))
   ([l k i]
     (logic/matche [l k]
       (['() i])
       ([[h r] _]
          (lengtho2 r k (inc i))))))

;; 1.03
(defn ktho
  ([l e k]
   (ktho l e k 0))

  ([l e k i]
     (logic/matche [l k]
       ([[e . r] i])
       ([[_ . r] _]
          (ktho r e k (inc i))))))

;; 1.05

(defn concato
  [l1 l2 cl]
  (logic/conde
   ((logic/emptyo l1)
    (logic/== l2 cl))
   ((logic/fresh [h r hc rc]
                 (logic/conso h r l1)
                 (logic/conso hc rc cl)
                 (logic/== h hc)
                 (concato r l2 rc)))))

(logic/defne concato2
  [l1 l2 cl]
  (['() l2 l2])
  ([[h . r] l2 [h . r2]]
     (concato2 r l2 r2)))


(defn reverso
  [l r]
  (logic/fresh [hd-l rest-l init-r last-r len-l len-r]
               ;; this addition hurts convergence, doesn't help it.
               ;; (lengtho l len-l)
               ;; (lengtho r len-r)
               ;; (logic/== len-l len-r)
               (logic/conso hd-l rest-l l)
               (concato init-r [hd-l] r)
               (logic/conde
                ((logic/emptyo rest-l)
                 (logic/emptyo init-r))
                ((reverso rest-l init-r)))))

(logic/defne reverso2 [l rev]
  (['() '()])
  ([[h . '()] [h . '()]])
  ([[h . r] _]
     (logic/fresh [init-rev]
       (initsegmento init-rev rev)
       (concato init-rev [h] rev)
       (reverso2 r init-rev))))

;; 1.06

(defn palindromo
  [l]
  (reverso l l))

;; 1.07

(defn notlisto
  [l]
  (logic/fresh [h t]
               (logic/conda
                ((logic/conso h t l)
                 logic/fail)
                (logic/succeed))))

(defn flato
  "checks if the input list is flat"
  [l]
  (logic/conda
   ((logic/emptyo l))
   ((notlisto l))
   ((logic/fresh [h t]
                 (logic/conso h t l)
                 (flato t)
                 (notlisto h)))))

(defn flatteno
  [l-in l-flat]
  (logic/conde
   ((flato l-in)
    (logic/== l-in l-flat))
   ((logic/fresh [l-head l-rest l-head-flat l-rest-flat]
                  (logic/conso l-head l-rest l-in)
                  (flatteno l-head l-head-flat)
                  (flatteno l-rest l-rest-flat)
                  (logic/conda
                   ((concato l-head-flat l-rest-flat l-flat))
                   ((notlisto l-head)
                    (logic/conso l-head l-rest-flat l-flat)))))))

;; 1.08

(defn dedupo
  ([l-in l-dedupped]
     (logic/fresh [l-head l-rest l-rest-dedup]
                  (logic/conso l-head l-rest l-in)
                  (logic/conde
                   ((logic/emptyo l-rest)
                    (logic/== l-in l-dedupped))
                   ((dedupo l-rest l-rest-dedup l-head)
                    (logic/conso l-head l-rest-dedup l-dedupped)))))
  ([l-in l-dedupped last-elt]
     (logic/fresh [l-head l-rest l-rest-dedup]
                  (logic/conso l-head l-rest l-in)
                  (logic/conde
                   ((logic/emptyo l-rest)
                    (logic/!= l-head last-elt)
                    (logic/== l-in l-dedupped))
                   ((logic/emptyo l-rest)
                    (logic/== l-head last-elt)
                    (logic/== nil l-dedupped))
                   ((dedupo l-rest l-rest-dedup l-head)
                    (logic/!= l-head last-elt)
                    (logic/conso l-head l-rest-dedup l-dedupped))
                   ((dedupo l-rest l-rest-dedup l-head)
                    (logic/== l-head last-elt)
                    (logic/== l-rest-dedup l-dedupped))))))

;; 1.09

(defn packdupo
  ([l lpacked]
     (logic/conde
      ((logic/emptyo l)
       (logic/emptyo lpacked))
      ((logic/fresh [lh lt pack]
                    (logic/conso lh lt l)
                    (logic/conso lh '() pack)
                    (packdupo lt lpacked pack)))))
  ([l lpacked pack]
     (logic/conde
      ((logic/emptyo l)
       (logic/conso pack '() lpacked))
      ((logic/fresh [lh lt ph pt newpack]
                    (logic/conso lh lt l)
                    (logic/conso ph pt pack)
                    (logic/conde
                     ((logic/== lh ph)
                      (logic/conso lh pack newpack)
                      (packdupo lt lpacked newpack))
                     ((logic/!= lh ph)
                      (logic/conso lh '() newpack)
                      (logic/fresh [packedh packedt]
                                   (logic/conso packedh packedt lpacked)
                                   (logic/== packedh pack)
                                   (packdupo lt packedt newpack)))))))))

;; 1.10 / 1.12 / 1.13

(defn runleno
  ([l rle]
     (logic/conde
      ((logic/emptyo l)
       (logic/emptyo rle))
      ((logic/fresh [h t]
                    (logic/conso h t l)
                    (runleno t rle h 1)))))
  ([l rle elem ct]
     (logic/conde
      ((logic/emptyo l)
       (logic/fresh [pack pelem]
                    (logic/conso elem nil pelem)
                    (logic/conso ct pelem pack)
                    (logic/conso pack nil rle)))
      ((logic/fresh [h t]
                    (logic/conso h t l)
                    (logic/conde
                     ((logic/== h elem)
                      (runleno t rle elem (inc ct)))
                     ((logic/!= h elem)
                      (logic/fresh [rle-t pack pelem]
                                   (runleno t rle-t h 1)
                                   (logic/conso elem nil pelem)
                                   (logic/conso ct pelem pack)
                                   (logic/conso pack rle-t rle)))))))))

;; 1.11

(defn fixrle
  [rle mrle]
  (logic/conde
   ((logic/emptyo rle)
    (logic/emptyo mrle))
   ((logic/fresh [h t hh tt f s ss]
                 (logic/conso h t rle)
                 (logic/conso f s h)
                 (logic/conso ss nil s)
                 (logic/conso hh tt mrle)
                 (logic/conde
                  ((logic/== f 1)
                   (logic/== hh ss)
                   (notlisto ss))    ;; note: solution does not work with elts being lists
                  ((logic/!= f 1)
                   (logic/== hh h)))
                 (fixrle t tt)))))

(defn mrunleno
  ([l mrle]
     (logic/fresh [rle]
                  (runleno l rle)
                  (fixrle rle mrle))))

;; 1.14 / 1.15

(defn repo
  "succeeds if l is (repeat num elem)"
  ([elem num l]
     (repo elem num l 0))
  ([elem num l i]
     (logic/conde
      ((logic/== num i)
       (logic/emptyo l))
      ((logic/fresh [h t]
                    (logic/conso h t l)
                    (logic/== h elem)
                    (repo elem num t (inc i)))))))

(defn multelto
  [l l-dupped i]
  (logic/fresh [h t hdup lt]
               (logic/conso h t l)
               (repo h i hdup)
               (concato hdup lt l-dupped)
               (logic/conde
                ((logic/emptyo lt)
                 (logic/emptyo t))
                ((multelto t lt i)))))

(defn dupelto
  [l l-dupped]
  (multelto l l-dupped 2))

;; 1.16

(defn dropntho
  ([l ldropped i]
     (dropntho l ldropped i 0))
  ([l ldropped i ct]
     (logic/conde
      ((logic/emptyo l)
       (logic/emptyo ldropped))
      ((logic/== i ct)
       (logic/fresh [h t]
                    (logic/conso h t l)
                    (dropntho t ldropped i 0)))
      ((logic/!= i ct)
       (logic/fresh [h t tt]
                    (logic/conso h t l)
                    (logic/conso h tt ldropped)
                    (dropntho t tt i (inc ct)))))))


;; 1.18

(defn sliceo
  ([l slice istart iend]
     (sliceo l slice istart iend 0 false))
  ([l slice istart iend ct take]
     (logic/conde


      ((logic/== take false)
       (logic/== istart ct)
       (sliceo l slice istart iend ct true))

      ((logic/== take false)
       (logic/!= istart ct)
       (logic/fresh [h t]
                    (logic/conso h t l)
                    (sliceo t slice istart iend (inc ct) false)))

      ((logic/== take true)
       (logic/fresh [h t st]
                    (logic/conso h t l)
                    (logic/conso h st slice)
                    (logic/conde
                     ((logic/== iend ct)
                      (logic/emptyo st))
                     ((logic/!= iend ct)
                      (sliceo t st istart iend (inc ct) true))))))))

;; 1.17

(defn splito
  ([l len pair]
     (logic/fresh [a b eb]
                  (logic/conso b nil eb)
                  (logic/conso a eb pair)
                  (splito l len a b)))
  ([l len a b]
     (logic/all
      (sliceo l a 0 len)
      (concato a b l))))

;; 1.19

(defn rotato
  [l lrot n]
  (logic/fresh [pair]
               (splito l n pair)
               (logic/fresh [a b eb]
                            (logic/conso b nil eb)
                            (logic/conso a eb pair)
                            (concato b a lrot))))

;; 1.20

(defn removektho
  ([l lrem k]
     (removektho l lrem k 0))

  ([l lrem k ct]
     (logic/conde
      ((logic/emptyo l)
       (logic/emptyo lrem))

      ((logic/== k ct)
       (logic/fresh [h t]
                    (logic/conso h t l)
                    (logic/== t lrem)))

      ((logic/!= k ct)
       (logic/fresh [h t trem]
                    (logic/conso h t l)
                    (logic/conso h trem lrem)
                    (removektho t trem k (inc ct)))))))

;; 1.21

(defn insertktho
  ([l lins inselem k]
     (insertktho l lins inselem k 0))
  ([l lins inselem k ct]
     (logic/conde

      ((logic/== k ct)
       (logic/conso inselem l lins))

      ((logic/!= k ct)
       (logic/fresh [h t tins]
                    (logic/conso h t l)
                    (logic/conso h tins lins)
                    (insertktho t tins inselem k (inc ct)))))))

;; 1.22

(defn rangeo
  ([ran istart iend]
     (rangeo ran istart iend 0 false))

  ([ran istart iend ct take]
     (logic/conde
      ((logic/== take false)
       (logic/== istart ct)
       (rangeo ran istart iend ct true))

      ((logic/== take false)
       (logic/!= istart ct)
       (rangeo ran istart iend (inc ct) false))

      ((logic/== take true)
       (logic/!= iend ct)
       (logic/fresh [t]
                    (logic/conso ct t ran)
                    (rangeo t istart iend (inc ct) true)))

      ((logic/== take true)
       (logic/== iend ct)
       (logic/conso ct nil ran)))))

;; 1.23

(defn rand-ntho
  ([l v]
     (logic/fresh [h t]
                  (logic/conso h t l)
                  (logic/conde
                   ((logic/emptyo t)
                    (logic/== v h))
                   ((rand-ntho t v h 1)))))
  ([l v cand ct]
     (logic/fresh [h t coin]
                  (logic/== coin (< (rand) (/ ct (inc ct))))
                  (logic/conso h t l)
                  (logic/conde
                   ((logic/== coin true)
                    (logic/conde
                     ((logic/emptyo t)
                      (logic/== v cand))
                     ((rand-ntho t v cand (inc ct)))))
                   ((logic/== coin false)
                    (logic/conde
                     ((logic/emptyo t)
                      (logic/== v h))
                     ((rand-ntho t v h (inc ct)))))))))

(comment
  "running the following shows that this function chooses fairly uniformly"
  (frequencies (repeatedly 4000 (fn [] (logic/run 1 [q] (rand-ntho [1 2 3 4] q)))))

core.logic99.core> (logic/run 2 [q] (rand-ntho q 4))
((4) (4 _0))
core.logic99.core> (logic/run 5 [q] (rand-ntho q 4))
((4) (4 _0) (_0 _1 4) (_0 _1 4 _2) (_0 _1 4 _2 _3))
core.logic99.core> (logic/run 7 [q] (rand-ntho q 4))
((4) (4 _0) (_0 _1 4) (_0 _1 4 _2) (_0 _1 4 _2 _3) (_0 _1 _2 _3 _4 4) (_0 _1 _2 _3 _4 _5 4))

core.logic99.core> (logic/run 1 [q] (rand-ntho [:a :b :c :d] q))
(:a)


)

(defn randselo
  ([l sel k]
     (randselo l sel k 1))
  ([l sel k ct]
     (logic/fresh [rl rem tsel]
                  (rand-ntho l rl)
                  (logic/rembero rl l rem)
                  (logic/conso rl tsel sel)
                  (logic/conde
                   ((logic/== k ct)
                    (logic/emptyo tsel))
                   ((randselo rem tsel k (inc ct)))))))

(comment

"the following looks fairly uniform"
(frequencies (repeatedly 10000 (fn [] (logic/run* [q] (randselo [1 2 3 4 5 6 7 8 9] q 2))))))

;; 1.24

(defn lottoo
  ([i k sel]
     (logic/fresh [ran]
                  (rangeo ran 0 i)
                  (randselo ran sel k))))

;; 1.25

(defn randpermo
  ([l perm]
     (logic/conde
      ((logic/emptyo l)
       (logic/emptyo perm))
      ((logic/fresh [lrem rn tperm]
                    (rand-ntho l rn)
                    (logic/rembero rn l lrem)
                    (logic/conso rn tperm perm)
                    (randpermo lrem tperm)
                    )))))

(comment
(frequencies (repeatedly 200 (fn [] (logic/run 1 [q] (randpermo [1 2] q))))))

;; 1.26

(defn combo
  ([l com len]
     (combo l com len 0))
  ([l com len ct]
     (logic/conde
      ((logic/== len ct)
       (logic/nilo com))
      ((logic/!= len ct)
       (logic/fresh [h t]
                       (logic/conso h t l)
                       (logic/conde
                        ((logic/fresh [tcom]
                                      (logic/conso h tcom com)
                                      (combo t tcom len (inc ct))))
                        ((combo t com len ct))))))))

(defn combo2
  ([l com len]
     (combo2 l com len []))

  ([l com len acc]
     (logic/conda
      ((lengtho acc len)
       (logic/== com acc))
      ((logic/fresh [h t]
                    (logic/conso h t l)
                    (logic/conde
                     ((combo2 t com len acc))
                     ((logic/fresh [newacc]
                                   (logic/conso h acc newacc)
                                   (combo2 t com len newacc)))))))))


;; 1.27


(defn addelemo
  [elem acc newacc sizes]
  (logic/fresh [ha hna hs ta tna ts len]
               (logic/conso ha ta acc)
               (logic/conso hna tna newacc)
               (logic/conso hs ts sizes)
               (lengtho ha len)
               (logic/conde
                ((logic/== len hs)             ; no room add later
                 (logic/== ha hna)
                 (addelemo elem ta tna ts))
                ((logic/!= len hs)
                 (logic/conso elem ha hna)     ; add here
                 (logic/== ta tna))
                ((logic/!= len hs)
                 (logic/== ha hna)             ; add later
                 (addelemo elem ta tna ts)))))

(defn groupo
  ([l groups sizes]
     (groupo l groups sizes (repeat (count sizes) [])))

  ([l groups sizes acc]
     (logic/conde
      ((logic/emptyo l)
       (logic/== groups acc))
      ((logic/fresh [hl tl newacc]
                    (logic/conso hl tl l)
                    (addelemo hl acc newacc sizes)
                    (groupo tl groups sizes newacc))))))

;; 1.28

(defn shortero
  [ls ll]
  (logic/conde
   ((logic/emptyo ls))
   ((logic/fresh [hls tls hll tll]
                 (logic/conso hls tls ls)
                 (logic/conso hll tll ll)
                 (shortero tls tll)))))

(defn insertsorto
  [elem l-in l-out]
  (logic/conde
   ((logic/emptyo l-in)
    (logic/conso elem '() l-out))
   ((logic/fresh [h t hh tt]
                 (logic/conso h t l-in)
                 (logic/conso hh tt l-out)
                 (logic/conda
                  ((shortero elem h)
                   (logic/== elem hh)
                   (logic/== l-in tt))
                  ((insertsorto elem t tt)
                   (logic/== h hh)))))))

(defn sortsizeo
  ([l lsorted]
     (sortsizeo l lsorted []))
  ([l lsorted acc]
     (logic/conde
      ((logic/emptyo l)
       (logic/== lsorted acc))
      ((logic/fresh [h t newacc]
                    (logic/conso h t l)
                    (insertsorto h acc newacc)
                    (sortsizeo t lsorted newacc))))))
