(ns core.logic99.arith
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use midje.sweet)
  (:gen-class))

(matrix/set-current-implementation :vectorz)


;; 2.01

(defn factors
  [p q x]
  (logic/all
   (fd/in p q (fd/interval 2 x))
   (fd/* p q x)))

(defn not-prime?
  [x]
  (logic/fresh [p q]
               (factors p q x)))

(defn prime?
  [x]
  (zero? (count (logic/run* [p] (not-prime? x)))))

(defn primo
  [x]
  (logic/fresh [p q]
               (fd/in p q (fd/interval 2 x))
               (logic/conda
                ((fd/* p q x)
                 logic/fail)
                (logic/succeed))))

;; 2.02

(defn list-factors
  [x lfac]
  (logic/fresh [h t]
               (logic/conso h t lfac)
               (fd/in h (fd/interval 2 x))
               (logic/fresh [q]
                            (factors h q x)
                            (list-factors q t))))

(defn list-prime-factors
  [x prime-factors]
  (logic/fresh [h t]
               (fd/in h (fd/interval 2 x))
               (logic/conso h t prime-factors)
               (logic/conde
                ((not-prime? h)
                 logic/fail)
                ((logic/fresh [quot]
                              (fd/in quot (fd/interval 2 x))
                              (factors h quot x)
                              (list-prime-factors quot t))))))
