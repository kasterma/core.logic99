(ns core.logic99.bintrees
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use midje.sweet
        core.logic99.core)
  (:gen-class))

(matrix/set-current-implementation :vectorz)

;; Here we represent trees as nested lists
;; [A B C] is a node with label A and children B, and C.
;; we mostly don't restrict labels

;; 4.01

(defn leaf?
  [node]
  (logic/fresh [label child1 child2]
               (logic/== [label child1 child2] node)
               (logic/nilo child1)
               (logic/nilo child2)))

(defn leaf [x]
  [x nil nil])

(defn bintree?
  [tree]
  (logic/conde
   ((logic/nilo tree))
   ((logic/fresh [label child1 child2]
                 (logic/== [label child1 child2] tree)
                 (bintree? child1)
                 (bintree? child2)))))

;; 4.02

(def info :x)

(defn balbintree
  [tree num]
  (let [ran   (range 0 num)]
    (logic/conde
     ((logic/== num 0)
      (logic/== tree nil))

     ((logic/fresh [n1 n2]
                   (logic/membero n1 ran)
                   (logic/membero n2 ran)
                   (logic/project [n1 n2]
                                  (logic/== num (+ n1 n2 1))
                                  (logic/conde
                                   ((logic/== n1 n2))
                                   ((logic/== n1 (+ n2 1)))
                                   ((logic/== n2 (+ n1 1))))
                                  (logic/fresh [t1 t2]
                                               (logic/== [info t1 t2] tree)
                                               (balbintree t1 n1)
                                               (balbintree t2 n2))))))))

;; 4.03

(defn mirror
  [tree1 tree2]
  (logic/conde
   ((logic/nilo tree1)
    (logic/nilo tree2))
   ((logic/fresh [la ca1 ca2 lb cb1 cb2]
                 (logic/== [la ca1 ca2] tree1)
                 (logic/== [lb cb1 cb2] tree2)
                 (mirror ca1 cb2)
                 (mirror ca2 cb1)))))


(defn symmbintree?
  [tree]
  (mirror tree tree))

;; 4.04

(defn insertnum
  [num tree tree-added]
  (logic/conde
   ((logic/fresh [l c1 c2 la ca1 ca2]
                  (logic/== [l c1 c2] tree)
                  (logic/== [la ca1 ca2] tree-added)
                  (logic/project [num l]
                                 (logic/conda
                                  ((logic/== true (> l num))
                                   (insertnum num c1 ca1)
                                   (logic/== l la)
                                   (logic/== c2 ca2))
                                  ((logic/== l num) ;; was already there
                                   (logic/== tree tree-added))
                                  ((logic/== true (> num l))
                                   (insertnum num c2 ca2)
                                   (logic/== l la)
                                   (logic/== c1 ca1))))))
   ((logic/nilo tree)
    (logic/== (leaf num) tree-added))))

(defn insertnum
  [num tree tree-added]
  (logic/matche [tree tree-added]
                ([[l c1 c2] [la ca1 ca2]]
                   (logic/project [num l]
                                  (logic/conda
                                   ((logic/== true (> l num))
                                    (insertnum num c1 ca1)
                                    (logic/== l la)
                                    (logic/== c2 ca2))
                                   ((logic/== l num) ;; was already there
                                    (logic/== tree tree-added))
                                   ((logic/== true (> num l))
                                    (insertnum num c2 ca2)
                                    (logic/== l la)
                                    (logic/== c1 ca1)))))
                ([nil _]
                   (logic/== (leaf num) tree-added))))

(defn searchtree
  ([num-list tree]
     (searchtree num-list tree nil))

  ([num-list tree acc]
     (logic/conde
      ((logic/emptyo num-list)
       (logic/== acc tree))
      ((logic/fresh [h t new-acc]
                    (logic/conso h t num-list)
                    (insertnum h acc new-acc)
                    (searchtree t tree new-acc))))))


;; 4.08

(defn leafcounto
  "note: we do not count subtrees of the form (leaf 3) = [3 nil nil],
   but the number of tree leaves.  (leaf 3) is a leaf in the node with
   information sense"
  [tree ct]
  (logic/conde
   ((logic/nilo tree)
    (logic/== ct 1))
   ((logic/fresh [lab c1 c2 ct1 ct2]
             (logic/== [lab c1 c2] tree)
             (leafcounto c1 ct1)
             (leafcounto c2 ct2)
             (logic/project [ct1 ct2]
                        (logic/== ct (+ ct1 ct2)))))))
