(ns core.logic99.log
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.core.logic.fd :as fd])
  (:use midje.sweet
        core.logic99.core)
  (:gen-class))

(matrix/set-current-implementation :vectorz)

;; 3.01

(defn AND [a b c]
  (logic/all
   (logic/== a true)
   (logic/== b true)
   (logic/== c true)))

(defn OR [a b c]
  (logic/conde
   ((logic/== a true)
    (logic/== c true))
   ((logic/== b true)
    (logic/== c true))))

(defn VAL [a]
  (logic/conde
   ((logic/== a true))
   ((logic/== a false))))

;; for simplicity do herbrandization only with binary operators.
(defmacro herbrandize [exp outvar]
  (if (seq? exp)
    (let [rator  (first exp)
          rand1  (second exp)
          rand2  (second (rest exp))]
      (cond
       (and (seq? rand1) (seq rand1) (seq? rand2) (seq rand2))
       (let [rand1-out  (gensym "rand1_")
             rand2-out  (gensym "rand2_")]
         `(logic/fresh [~rand1-out ~rand2-out]
                       (~rator ~rand1-out ~rand2-out ~outvar)
                       (herbrandize ~rand1 ~rand1-out)
                       (herbrandize ~rand2 ~rand2-out)))

       (and (seq? rand1) (seq rand1))
       (let [rand1-out  (gensym "rand1_")]
         `(logic/fresh [~rand1-out]
                       (~rator ~rand1-out ~rand2 ~outvar)
                       (herbrandize ~rand1 ~rand1-out)))

       (and (seq? rand2) (seq rand2))
       (let [rand2-out  (gensym "rand2_")]
         `(logic/fresh [~rand2-out]
                       (~rator ~rand1 ~rand2-out ~outvar)
                       (herbrandize ~rand2 ~rand2-out)))

       :default
       `(~rator ~rand1 ~rand2 ~outvar)))))


(defmacro table
  [a b exp]
  `(distinct (logic/run* [~a ~b c#] (VAL ~a) (VAL ~b) (VAL c#) (herbrandize ~exp c#))))

;; 3.04

(defn prependo
  [l pl elem]
  (logic/conde
   ((logic/emptyo l)
    (logic/emptyo pl))
   ((logic/fresh [h t ph pt]
                 (logic/conso h t l)
                 (logic/conso ph pt pl)
                 (logic/conso elem h ph)
                 (prependo t pt elem)))))

(defn graycodeo
  "only to be used with len an integer"
  [len code]
  (logic/conde
   ((logic/== len 1)
    (logic/== code [[0] [1]]))
   ((logic/fresh [scode rscode pscode prscode]
                 (graycodeo (dec len) scode)
                 (reverso scode rscode)
                 (prependo scode pscode 0)
                 (prependo rscode prscode 1)
                 (concato pscode prscode code)))))

;; 3.05

(defn insertfreqsorto
  [elem l-in l-out]
  (logic/conda
   ((logic/emptyo l-in)
    (logic/conso elem '() l-out))
   ((logic/fresh [h t hh tt]
                 (logic/conso h t l-in)
                 (logic/conso hh tt l-out)
                 (logic/conda
                  ((logic/fresh [helem freq-elem hdh freq-h]
                                (logic/== [helem freq-elem] elem)
                                (logic/== [hdh freq-h] h)
                                (logic/== elem hh)
                                (logic/== l-in tt)
                                (logic/project [freq-elem freq-h]
                                               (if (< freq-elem freq-h)
                                                 logic/succeed
                                                 logic/fail))))
                  ((insertfreqsorto elem t tt)
                   (logic/== h hh)))))))

(defn sortfreqs
  [freqs freqs-sorted]
  (logic/conde
   ((logic/emptyo freqs)
    (logic/emptyo freqs-sorted))
   ((logic/fresh [h t t-sorted]
                 (logic/conso h t freqs)
                 (sortfreqs t t-sorted)
                 (insertfreqsorto h  t-sorted freqs-sorted)))))

(defn combinesmallest
  [freqs freqs-combined]
  (logic/fresh [freqs-merge h1 f1 h2 f2 f-new t tt]
               (logic/conso [h1 f1] t freqs)
               (logic/conso [h2 f2] tt t)
               (logic/project [f1 f2]
                              (logic/== f-new (+ f1 f2)))
               (logic/conso [[h1 h2] f-new] tt freqs-merge)
               (sortfreqs freqs-merge freqs-combined)))

(defn make-tree
  [freqs tree]
  (logic/conde
   ((logic/fresh [f]
                 (lengtho freqs 1)
                 (logic/== [[tree f]] freqs)))
   ((logic/fresh [fc]
                 (combinesmallest freqs fc)
                 (make-tree fc tree)))))

(defn add-label
  [l l-relab elem]
  (logic/conde
   ((logic/emptyo l)
    (logic/emptyo l-relab))
   ((logic/fresh [x xl nxl tt ttt]
                 (logic/conso [x xl] tt l)
                 (logic/conso elem xl nxl)
                 (logic/conso [x nxl] ttt l-relab)
                 (add-label tt ttt elem)))))

(defn read-tree
  [tree code]
  (logic/fresh [a b c d c1 c2 c1r c2r]
               (logic/conda
                ((logic/== [[a b] [c d]] tree)
                 (read-tree [a b] c1)
                 (read-tree [c d] c2)
                 (add-label c1 c1r 0)
                 (add-label c2 c2r 1)
                 (concato c1r c2r code))

                ((logic/== [a [b c]] tree)
                 (read-tree [b c] c1)
                 (add-label c1 c1r 1)
                 (concato [[a [0]]] c1r code))

                ((logic/== [[a b] c] tree)
                 (read-tree [a b] c1)
                 (add-label c1 c1r 0)
                 (concato c1r [[c [1]]] code))

                ((logic/== [a b] tree)
                 (logic/== [[a [0]] [b [1]]] code))

                ((logic/== [a] tree)
                 (logic/== [[a [0]]] code)))))

(defn huffmancodeo
  [freqs codes]
  (logic/fresh [freqs-sorted tree]
               (sortfreqs freqs freqs-sorted)
               (make-tree freqs-sorted tree)
               (read-tree tree codes)))
