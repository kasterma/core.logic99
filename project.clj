(defproject core.logic99 "0.1.0-SNAPSHOT"
  :description "running through a list of prolog problems in core.logic"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/data.json "0.2.2"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [org.clojure/tools.cli "0.2.2"]
                 [midje "1.5.0"]
                 [org.clojure/tools.trace "0.7.5"]
                 [com.taoensso/timbre "1.6.0"]
                 [incanter "1.4.1"]
                 [net.mikera/vectorz-clj "0.9.0"]
                 [org.clojure/tools.cli "0.2.2"]
                 [org.clojure/core.logic "0.8.3"]
                 [net.mikera/core.matrix "0.6.0"]]
  :jvm-opts ["-XX:MaxPermSize=128M"
             "-XX:+UseConcMarkSweepGC"
             "-Xms2g" "-Xmx2g" "-server"]
  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]])
